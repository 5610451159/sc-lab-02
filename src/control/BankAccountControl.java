package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.BankAccount;
import view.BankAccountGUI;

public class BankAccountControl {
	/* ���͡��� object �ͧ BankAccount ���  BankAccountGUI �� attribute �����繵���÷�������� class */
	private static final double INITIAL_BALANCE = 1000;  
	private static BankAccountGUI GUI = new BankAccountGUI(); //
	private static BankAccount account;
	
	class AddInterestListener implements ActionListener {
    	public void actionPerformed(ActionEvent event) {
    		double rate = Double.parseDouble(GUI.getTextRate());
            double interest = account.getBalance() * rate / 100;
            account.deposit(interest);
            GUI.getResultLabel("balance : " + account.getBalance());
        }            
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new BankAccountControl();
	}	
	
	public BankAccountControl() {
		account = new BankAccount(INITIAL_BALANCE);
		listener = new AddInterestListener();
		GUI.InvestmentFrame(account.getBalance());
		GUI.createTextField();
		GUI.createButton(listener);
		GUI.createPanel();
	}
	
	ActionListener listener;
	
}
